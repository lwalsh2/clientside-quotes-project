import json, http.client, urllib

global connection
global AppID
global APIKey
connection = http.client.HTTPSConnection('api.parse.com', 443)
AppID = "nXoPAKNT9k8OLsx7c1dFWpxD1VU9klFxLlUtzYUH"
APIKey = "O6YjvFywjwRneB5ykzpoYsNlWO6nwvGMqGiq7mGc"

#Get specified account and its corresponding data
def get_account(username):
	encAccount = urllib.parse.urlencode({"where":json.dumps({"name":username})})
	connection.connect()
	connection.request('GET', '/1/classes/Account?%s' % encAccount, '', {
		"X-Parse-Application-Id": AppID,
		"X-Parse-REST-API-Key": APIKey
	})
	return json.loads(connection.getresponse().read().decode('utf-8'))

#similar to above, but comparse both name and password variables to validate an account.
def get_account_password(username, password):
	encAccount = urllib.parse.urlencode({"where":json.dumps({"name":username,"password":password})})
	connection.connect()
	connection.request('GET', '/1/classes/Account?%s' % encAccount, '', {
		"X-Parse-Application-Id": AppID,
		"X-Parse-REST-API-Key": APIKey
	})
	return json.loads(connection.getresponse().read().decode('utf-8'))

def update_account_password(username, password):
	#convert to usable Object ID
	acc = get_account(username)
	acc = acc['results']
	acc = acc[0]
	acc = acc['objectId']
	
	connection.connect()
	connection.request('PUT', '/1/classes/Account/' + acc, json.dumps({"password": password}), {
		"X-Parse-Application-Id": AppID,
		"X-Parse-REST-API-Key": APIKey,
		"Content-Type": "application/json"
	})
	return( json.loads(connection.getresponse().read().decode('utf-8')))

	
#Gets all account information from Parse
#Returns: Array of Account objects from Parse
def get_accounts():
	connection.connect()
	connection.request('GET', '/1/classes/Account', '', {
		"X-Parse-Application-Id": AppID,
		"X-Parse-REST-API-Key": APIKey
	})
	return json.loads(connection.getresponse().read().decode('utf-8'))
	
#Creates a new user in parse
#Params: two strings, a name and a password
#returns: Array with one element containing information from the created account
def create_account(name, password):
	connection.connect()
	connection.request('POST', '/1/classes/Account', json.dumps({
	"name":name,
	"password":password
	}),{
		"X-Parse-Application-Id": AppID,
		"X-Parse-REST-API-Key": APIKey
	})
	return json.loads(connection.getresponse().read().decode('utf-8'))
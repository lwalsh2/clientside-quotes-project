import json, http.client, urllib

global connection
global AppID
global APIKey
connection = http.client.HTTPSConnection('api.parse.com', 443)
AppID = "nXoPAKNT9k8OLsx7c1dFWpxD1VU9klFxLlUtzYUH"
APIKey = "O6YjvFywjwRneB5ykzpoYsNlWO6nwvGMqGiq7mGc"
	
#Creates a new quote, tied with a name
#Params: two strings, a name and the quote
#returns: Array with one element containing information from the quote.
def create_quote(name, quote):
	connection.connect()
	connection.request('POST', '/1/classes/Quote', json.dumps({
	"name":name,
	"quote":quote,
	"likes":0,
	"dislikes":0
	}),{
		"X-Parse-Application-Id": AppID,
		"X-Parse-REST-API-Key": APIKey
	})
	return json.loads(connection.getresponse().read().decode('utf-8'))

#Parameter: quoteID, whether to icrement like or dislike field
#Add like = true
#add dislike = false
def update_quote(quoteID, addLike):	
	connection.connect()
	like = ""
	if(addLike):
		like = "likes"
	else:
		like = "dislikes"
	
	connection.request('PUT', '/1/classes/Quote/' + quoteID, json.dumps({
		like: {
			"__op": "Increment",
			"amount": 1
		}
	}), {
		"X-Parse-Application-Id": AppID,
		"X-Parse-REST-API-Key": APIKey,
		"Content-Type": "application/json"
	})	
	return( json.loads(connection.getresponse().read().decode('utf-8')))
	
#returns quotes that were posted by a user
def get_user_quotes(name):
	params = urllib.parse.urlencode({"where":json.dumps({"name":name})})
	return get_quote_params(params)
	
def get_quote(qID):
	connection.connect()
	connection.request('GET', '/1/classes/Quote/' + qID, '', {
		"X-Parse-Application-Id": AppID,
		"X-Parse-REST-API-Key": APIKey
	})
	return json.loads(connection.getresponse().read().decode('utf-8'))
	
#similar to get_accounts(), returns an array of quote objects
def get_quotes():
	connection.connect()
	connection.request('GET', '/1/classes/Quote', '', {
		"X-Parse-Application-Id": AppID,
		"X-Parse-REST-API-Key": APIKey
	})
	return json.loads(connection.getresponse().read().decode('utf-8'))

# Returns array of quotes sorted by "key" column in parse - to reverse order of sort, put "-" in front key
def get_sorted_quotes(key, limit):
	params = urllib.parse.urlencode({"limit":limit,"order":key})
	return get_quotes_params(params)
	
def get_quotes_params(params):
	connection.connect()
	connection.request('GET', '/1/classes/Quote?%s' % params, '', {
		"X-Parse-Application-Id": AppID,
		"X-Parse-REST-API-Key": APIKey
	})
	return json.loads(connection.getresponse().read().decode('utf-8'))
	
#Gets a random quote from our quotes. Then returns it as a string.
#DONT PUT 'from random import randrange' AT THE TOP.
#I had some weird stuff going on and throwing exceptions about that function
#existing (even though I had imported random). Leave it here until 
#I figure out what's going on.
def get_random_quote():
	from random import randrange
	quote = get_quotes()
	quote = quote['results']
	arraySize = len(quote)
	rand = randrange(0, arraySize)
	quote = quote[rand]
	return quote